const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const mongoose = require('mongoose')

class UserExistsError extends Error {
  constructor(user) {
    super(`user "${user}" is already registered in database`);
    this.name = 'UserExistsError';
  }
}

class UnknownIdError extends Error {
  constructor(id) {
    super(`id "${id}" not found in database`);
    this.name = 'UnknownIdError';
  }
}


class MissingFieldError extends Error {
  constructor(id) {
    super('missing form field in post request');
    this.name = 'MissingFieldError';
  }
}

mongoose.connect(
  process.env.MONGO_URI,
  { useNewUrlParser: true, useUnifiedTopology: true },
  function(err) {
    if (err) {
      console.log(err);
      throw err;
    }
  }
);

const exerciseSchema = new mongoose.Schema({
  description: String,
  duration: Number,
  date: Date,
});

const userSchema = new mongoose.Schema({
  name: String,
  exercises: [exerciseSchema],
  count: Number,
});

const Exercise = mongoose.model('Exercise', exerciseSchema);
const User = mongoose.model('User', userSchema);

const app = express();

app.use(cors())

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


app.use(express.static('public'))
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/views/index.html')
});

app.post('/api/exercise/new-user', (req, res, next) => {
  const username = req.body.username;
  User.findOne({ name: username }, (err, user) => {
    if (err) return next(err);
    if (user) {
      return next(new UserExistsError(username));
    }
    new User({ name: username, exercises: [], count: 0 })
      .save()
      .then((x) => res.json(x))
      .catch((err) => next(err));
  });
});

app.get('/api/exercise/users', (req, res, next) => {
  User.find({}, (err, users) => {
    if (err) return next(err);
    res.json(users);
  });
});

app.post('/api/exercise/add', (req, res, next) => {
  const { userId, description, duration } = req.body;
  if (!userId || !description || !duration) {
    return next(new MissingFieldError());
  }
  const date = req.body.date || new Date();
  const newExercise = new Exercise({ description, duration, date });
  User.findOneAndUpdate({ _id: userId },
                        { $push: { exercises: newExercise }, $inc: { count: 1 }},
                        { new: true },
                        (err, user) => {
    if (err) return next(err);
    if (!userId) return next(new UnknownIdError(userId));
    res.json(user);
  });
});

app.get('/api/exercise/log/:userId', (req, res, next) => {
  const { userId } = req.params;
  const { from, to, limit=-1 } = req.query; 
  User.findOne({ _id: userId }, (err, user) => {
    if (err) return next(err);
    if (!user) return next(new UnknownIdError(userId));
    let dateFilters = [(d) => d];
    // Todo: maybe validate the dates before processing
    if (from) {
      dateFilters.push((d) => d >= new Date(from));
    }
    if (to) {
      dateFilters.push((d) => d <= new Date(to));
    }
    const dateFilter = (d) => {
      for (const f of dateFilters) {
        if (!f(d)) {
          return false;
        }
      }
      return d;
    };
    user.exercises = user.exercises.filter((ex) => dateFilter(ex.date));
    const noOverflowLimit = (limit > user.exercises.length) ? user.exercises.length: limit;
    user.exercises = user.exercises.slice(0, noOverflowLimit);
    res.json(user);
  });
});

app.use((req, res, next) => {
  return next({status: 404, message: 'not found'})
});

// Error Handling middleware
app.use((err, req, res, next) => {
  let errCode, errMessage

  if (err.errors) {
    // mongoose validation error
    errCode = 400 // bad request
    const keys = Object.keys(err.errors)
    // report the first validation error
    errMessage = err.errors[keys[0]].message
  } else {
    // generic or custom error
    errCode = err.status || 500
    errMessage = err.message || 'Internal Server Error'
  }
  res.status(errCode).type('txt')
    .send(errMessage)
});

const listener = app.listen(process.env.PORT || 3000, () => {
  console.log('Your app is listening on port ' + listener.address().port)
});
